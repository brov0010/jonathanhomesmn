<!DOCTYPE>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js" type="text/javascript"></script><![endif]-->
<?php wp_head(); ?>
</head>
<body>
<?php wp_nav_menu(array("menu" => "Main", "container" => "nav", "container_class" => "horizontal")); ?>